
## Table of contents
* [General info](#general-info)  
* [Technologies](#technologies)  
* [Setup](#setup)  



## General info
This project focused on analyzing socioeconomic factors and Twitter sentiment in relation to vaccination rates.

## Technologies
Project is created with the base languages:  

  * Python 3.7.3  
  * R 3.5.1   

Libraries and Packages:  

**Miscellaneous**  

  * Conda  


**Python**  
  
  * Pandas  
  * NumPy   
  * Scikit-Learn   
  * Statsmodels  
  * Matplotlib   
  * Seaborn   
  * Mpl_toolkits (part of matplotlib)  
  * Pylab (part of matplotlib)   
  * Glue  
  * Randomforest   
  * Scikit-learn  
  * Statsmodels  
  * Sqlite  
  * Seaborn   
  * Statsmodels   
  * geopandas  
  * cenpy  
  * twitter  
  * Rust  
  * Ffmpeg  
  * botometer  
  
  
**R**  
  
  * devtools  
  * remotes
  * rtweet   
  * Ggplot2   
  * Dplyr   
  * tm  
  * SnowballC  
  * Tidytext  
  * Stringr  
  * Tidyr  
  * fpc  
  * arules  
  * gganimate  
  * mpapproj  
  * ggmap  
  * maps  
  * rsconnect  
  * pryr  
  * gifski  
  * textfeatures  
  * syuzhet  
  * wordcloud  
  * hmisc  
  * rgraphviz  
  * graph  
  * reshape  
  * textfeatures  
  * tweetbotornot  
  * censusapi  
  * Rcolorbrewer  
  * tidycensus  
  * Stringi  
  * tidyselect  
  * tidyverse  
  * acs  

## Setup
To run this project, Run notebooks in the following order:

### DATA CARPENTRY
Packages for Twitter Install: https://c2019g09.dsa.missouri.edu/user/rossbr/notebooks/final-capstone/Twitter/Packages%20for%20Twitter%20Analysis%20Install.ipynb  
RTwitterWithoutBot: https://c2019g09.dsa.missouri.edu/user/rossbr/notebooks/final-capstone/Twitter/RTwitterWithoutBot.ipynb  
PYTweetWithoutBot: https://c2019g09.dsa.missouri.edu/user/rossbr/notebooks/final-capstone/Twitter/PYTweetWithoutBot.ipynb  
Toddler 2006: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2006.ipynb  
Toddler 2007: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2007.ipynb  
Toddler 2008: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2008.ipynb  
Toddler 2009: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2009.ipynb  
Toddler 2010: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2010.ipynb  
Toddler 2011: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2011.ipynb  
Toddler 2012: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2012.ipynb  
Toddler 2013: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2013.ipynb  
Toddler 2014: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2014.ipynb  
Toddler 2015: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2015.ipynb  
Toddler 2016: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2016.ipynb  
Toddler 2017: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2017.ipynb  
Toddler combined: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/toddler.ipynb  
Education: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/education.ipynb  
Exemption: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/exemption.ipynb  
Immunization Rate: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/imm_rate.ipynb  
Poverty: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/poverty.ipynb  
Toddler2: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/toddler2.ipynb  
Uninsured: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/uninsured.ipynb  
Combining Dataframes: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/Combining%20Dataframes.ipynb  
Rate of Change 2000 to 2017: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/Rate%20of%20Change%202000%20-%202017.ipynb  
Rate of Change 2010 to 2017: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/Rate%20of%20Change%202010%20-%202017.ipynb  
2000 to 2017 with Rate of Change and Twitter: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2000%20-%202017%20Data%20with%20Rate%20of%20Change%20and%20Twitter.ipynb  
2010 to 2017 Data with Rate of Change and Twitter: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/01%20Data%20Carpentry/2010%20-%202017%20Data%20with%20Rate%20of%20Change%20and%20Twitter.ipynb  
  
### DATA EXPLORATION  
Boxplots for Outliers: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/02%20Data%20Exploration/Boxplots.ipynb  
Linear Regression Immunization Rate vs Education: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/02%20Data%20Exploration/Linear%20Regression%20-%20Imm%20Rate%20Rate%20of%20Change%20vs%20Education%20Rate%20of%20Change.ipynb  
Linear Regression Immunization Rate vs Exemptions: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/02%20Data%20Exploration/Linear%20Regression%20-%20Imm%20Rate%20Rate%20of%20Change%20vs%20Exemption%20Rate%20of%20Change.ipynb  
Linear Regression Immunization Rate vs Insurance: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/02%20Data%20Exploration/Linear%20Regression%20-%20Imm%20Rate%20Rate%20of%20Change%20vs%20Insurance%20Rate%20of%20Change.ipynb  
Linear Regression Immunization Rate vs Poverty: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/02%20Data%20Exploration/Linear%20Regression%20-%20Imm%20Rate%20Rate%20of%20Change%20vs.%20Percent%20Poverty%20Rate%20of%20Change.ipynb  
Linear Regression Immunization Rate vs Total Population:https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/02%20Data%20Exploration/Linear%20Regression%20-%20Imm%20Rate%20RoC%20vs%20Total%20Pop%20RoC.ipynb  
Linear Regression Immunization Rate vs Year: https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/02%20Data%20Exploration/Linear%20Regression%20-%20Immunization%20Rate%20vs%20Year.ipynb  
Linear Regression Immunization Rate vs Immunization Rate Rate of Change : https://c2019g09.dsa.missouri.edu/user/moumc/notebooks/final-capstone/02%20Data%20Exploration/Linear%20Regression%20-%20Immunization%20Rate%20vs.%20Imm%20Rate%20Rate%20of%20Change.ipynb  

### NORMALIZATION
Socioeconomic: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Normalization/Normalizing%20Socioeconomic%20Data.ipynb
Socioeconomic and Twitter: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Normalization/Normalizing%20Socioeconomic%20and%20Twitter.ipynb
Socioeconomic Validation: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Normalization/Normalizing%20Socioeconomic%20Validation%20Data.ipynb
Socioeconomic and Twitter Validation: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Normalization/Normalizing%20Socioeconomic%20and%20Twitter%20Validation%20Data.ipynb

### MULTICOLLINEARITY 
Socioeconomic: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Multicollinearity/Multicolinearity_SocioeconomicData.ipynb
Twitter: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Multicollinearity/Multicolinearity_Twitter.ipynb
Socioeconomic and Twitter: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Multicollinearity/Multicolinearity_SocioeconomicandTwitter.ipynb

### MANOVA/ANOVA
Socioeconomic:
    50%: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/MANOVA/MANOVA_50MulticollinearitySocioeconomic.ipynb 
    20%: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/MANOVA/MANOVA_20MulticollinearitySocioeconomic.ipynb
    5%: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/MANOVA/MANOVA_5MulticollinearitySocioeconomic.ipynb
    2.5%: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/MANOVA/MANOVA_2.5MulticollinearitySocioeconomic.ipynb
Twitter:
    50%: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/MANOVA/MANOVA_50MulticollinearityTwitter.ipynb
    20%: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/MANOVA/MANOVA_20MulticollinearityTwitter.ipynb
    5%: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/MANOVA/MANOVA_5MulticollinearityTwitter.ipynb
    2.5%: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/MANOVA/MANOVA_2.5MulticollinearityTwitter.ipynb

### MODELS
Initial Model: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Models/InitialModel.ipynb
Analyzing all Socioeconomic possibilities: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Models/LogisticRegression_Socioeconomic_Allmodels.ipynb
Socioeconomic Final: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Models/SocioeconomicLogisticRegressionModel.ipynb
Socioeconomic with Rate of Change: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Models/SocioeconomicLogisticRegressionModel-RateofChangeVariables.ipynb
Analyzing all Twitter possibilities: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Models/LogisticRegression_Twitter_Allmodels.ipynb
Twitter Final: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Models/TwitterLogisticRegressionModel.ipynb
Combined Model (Socioeconomic and Twitter): https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Models/SocioeconomicandTwitterLogisticRegressionModel.ipynb
Validation Socioeconomic Model: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Models/SocioeconomicLogisticRegressionModel-Validation.ipynb
Validation Twitter Model: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Models/TwitterLogisticRegressionModel-Validation.ipynb
Validation Combined Model:https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Models/SocioeconomicandTwitterLogisticRegressionModel-Validation.ipynb

### VISUALIZATION  
Wordclouds: https://c2019g09.dsa.missouri.edu/user/rossbr/notebooks/final-capstone/Vis/RTwitterWordCloud.ipynb  
RTwitter Vis: https://c2019g09.dsa.missouri.edu/user/rossbr/notebooks/final-capstone/Vis/RTwitterVis.ipynb
Correlation Matrices: https://c2019g09.dsa.missouri.edu/user/rossbr/notebooks/final-capstone/Vis/CorrelationMatrix.ipynb

### RESULTS
Prediction dataframes: https://c2019g09.dsa.missouri.edu/user/rnac68/notebooks/final-capstone/Results/Predicted_CombinedDF.ipynb


